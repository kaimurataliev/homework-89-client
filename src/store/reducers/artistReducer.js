import {FETCH_ARTISTS_SUCCESS, FETCH_ARTISTS_ERROR} from '../actions/artistActions';

const initialState = {
    artists: [],
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.artists};

        case FETCH_ARTISTS_ERROR:
            return {...state, error: action.error};

        default:
            return state;
    }
};

export default reducer;

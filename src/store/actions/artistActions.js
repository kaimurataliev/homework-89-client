import axios from '../../axios';
import {push} from 'react-router-redux';

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_ERROR = 'FETCH_ARTISTS_ERROR';
export const ADD_ARTIST = 'ADD_ARTIST';

export const fetchArtistsSuccess = (artists) => {
    return {type: FETCH_ARTISTS_SUCCESS, artists}
};

export const fetchArtistsError = (error) => {
    return {type: FETCH_ARTISTS_ERROR, error};
};

export const fetchArtists = () => {
    return (dispatch) => {
        return axios.get('/artists')
            .then(response => {
                dispatch(fetchArtistsSuccess(response.data))
            }, error => {
                const errorObj = error.response ? error.response.data : "no internet";
                dispatch(fetchArtistsError(errorObj))
            })
    }
};

export const addArtist = (artist) => {
    return (dispatch, getState) => {
        const headers = {'Token' : getState().users.user.token};
        return axios.post('/artists', artist, {headers}).then(() => {
            dispatch(push('/'));
        })
    }
};


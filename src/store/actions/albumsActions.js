import axios from '../../axios';
import {push} from 'react-router-redux';

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const ADD_ALBUM = 'ADD_ALBUM';

export const fetchAlbumsSuccess = (data) => {
    return {type: FETCH_ALBUMS_SUCCESS, data};
};

export const fetchAlbums = (id) => {
    return dispatch => {
        return axios.get(`/albums?artist=${id}`)
            .then(response => {
                dispatch(fetchAlbumsSuccess(response.data))
            })
    }
};

export const addAlbum = (album) => {
    return (dispatch, getState) => {
        const headers = {'Token' : getState().users.user.token};
        return axios.post('/albums', album, {headers}).then(() => {
            dispatch(push('/'))
        })
    }
};


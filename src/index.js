import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware, routerReducer, ConnectedRouter} from 'react-router-redux';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import userReducer from './store/reducers/userReducer';
import artistReducer from './store/reducers/artistReducer';
import albumsReducer from './store/reducers/albumsReducer';
import tracksReducer from './store/reducers/tracksReducer';

const rootReducer = combineReducers({
    artists: artistReducer,
    users: userReducer,
    albums: albumsReducer,
    tracks: tracksReducer,
    routing: routerReducer
});

const history = createHistory();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const saveStateToLocalStorage = (state) => {
    try {
        const serializeState = JSON.stringify(state);
        localStorage.setItem('state', serializeState);
    } catch (e) {
        console.log("can't save to state");
    }
};

const loadStateFromLocalStorage = () => {
    try {
        const serializeState = localStorage.getItem('state');
        if(serializeState === null) {
            return undefined;
        }
        return JSON.parse(serializeState)
    } catch (e) {
        return undefined;
    }
};

const persistedState = loadStateFromLocalStorage();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(rootReducer,persistedState, enhancers);

store.subscribe(() => {
    saveStateToLocalStorage({
        users: store.getState().users
    })
});

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();

import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchHistoryTracks} from "../../store/actions/tracksActions";
import {ListGroup, ListGroupItem, Label} from "react-bootstrap";
import {Redirect} from 'react-router-dom';

class History extends Component {


    componentDidMount() {
        if (this.props.user && this.props.user.token) {
            this.props.fetchHistoryTracks(this.props.user.token)
        }
    }

    render() {
        if (!this.props.user || !this.props.user.token) {
            return <Redirect to="/register"/>
        }

        return (
            <Fragment>
                <Label bsStyle="primary" style={{fontSize: '20px'}}>Tracks history</Label>
                <ListGroup style={{marginTop: '20px'}}>
                    {this.props.history.map((track, index) => {
                        return (
                            <ListGroupItem key={index}>
                                <span style={{marginRight: '30px'}}>№: {track.track.number}</span>
                                <strong style={{marginRight: '30px'}}>{track.track.title}</strong>
                                <span style={{marginRight: '30px'}}>Artist: {track.track.artist.name}</span>
                                <span style={{marginRight: '30px'}}>Album: {track.track.album.title}</span>
                                <span style={{marginRight: '30px'}}>Listened at: {track.datetime}</span>
                            </ListGroupItem>
                        )
                    })}
                </ListGroup>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        history: state.tracks.historyTracks,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchHistoryTracks: (token) => dispatch(fetchHistoryTracks(token))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(History);
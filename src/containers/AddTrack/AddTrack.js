import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {fetchAlbums} from "../../store/actions/albumsActions";
import {addTrack} from "../../store/actions/tracksActions";

class AddTrack extends Component {

    state = {
        number: null,
        title: '',
        album: '',
        long: ''
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.addTrack(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    selectChangeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    render() {
        console.log(this.props.albums);
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormGroup controlId="number">
                    <Col componentClass={ControlLabel} sm={2}>
                        Number of track
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="number"
                            required
                            placeholder="Enter number"
                            name="number"
                            value={this.state.number}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="Enter title"
                            name="title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="album">
                    <Col componentClass={ControlLabel} sm={2}>
                        Choose album
                    </Col>
                    <Col sm={10}>
                        <FormControl value={this.state.performer}
                                     name='album'
                                     onChange={this.selectChangeHandler}
                                     componentClass="select"
                                     required
                        >
                            <option>Choose album</option>
                            {this.props.albums.map(album =>
                                <option
                                    key={album._id}
                                    value={album._id}
                                >
                                    {album.title}

                                </option>
                            )}
                        </FormControl>
                    </Col>
                </FormGroup>

            </Form>
        )
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAlbums: () => dispatch(fetchAlbums()),
        addTrack: (track) => dispatch(addTrack())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTrack)
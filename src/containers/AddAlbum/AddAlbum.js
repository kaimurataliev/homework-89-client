import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {addAlbum} from "../../store/actions/albumsActions";
import {fetchArtists} from "../../store/actions/artistActions";

class AddAlbum extends Component {

    componentDidMount() {
        this.props.fetchArtists();
    }

    state = {
        title: '',
        performer: '',
        year: '',
        image: null
    };

    submitFormHandler = event => {
        event.preventDefault();

        const albumData = new FormData();
        Object.keys(this.state).forEach(key => {
            albumData.append(key, this.state[key]);
        });
        this.props.addAlbum(albumData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    selectChangeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    render() {
        console.log(this.props.artists);
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="Enter title"
                            name="title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="artist">
                    <Col componentClass={ControlLabel} sm={2}>
                        Choose artist
                    </Col>
                    <Col sm={10}>
                        <FormControl value={this.state.performer}
                                     name='performer'
                                     onChange={this.selectChangeHandler}
                                     componentClass="select"
                                     required
                        >
                            <option>Choose artist</option>
                            {this.props.artists.map(artist =>
                                <option
                                    key={artist._id}
                                    value={artist._id}
                                >
                                    {artist.name}

                                </option>
                            )}
                        </FormControl>
                    </Col>
                </FormGroup>


                <FormGroup controlId="year">
                    <Col componentClass={ControlLabel} sm={2}>
                        Year
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="number"
                            required
                            placeholder="Enter year"
                            name="year"
                            value={this.state.year}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="image">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            required
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Add</Button>
                    </Col>
                </FormGroup>

            </Form>
        )
    }
}

const mapStateToProps = state => {
    return {
        artists: state.artists.artists
    }
};


const mapDispatchToProps = dispatch => {
    return {
        addAlbum: (album) => dispatch(addAlbum(album)),
        fetchArtists: () => dispatch(fetchArtists())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);

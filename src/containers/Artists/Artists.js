import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchArtists} from "../../store/actions/artistActions";
import {Panel, Image, Button, Label} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

class Artists extends Component {

    componentDidMount() {
        this.props.fetchArtists();
    };

    render() {
        return (
            <Fragment>
                <Label bsStyle="primary" style={{fontSize: '20px'}}>Artists</Label>
                {this.props.artists.map((artist, index) => {
                    return (
                        <Panel style={{marginTop: '20px'}} key={index}>
                            <Image
                                thumbnail
                                style={{width: '100px', marginRight: '50px'}}
                                src={'http://localhost:8000/uploads/' + artist.image}
                            />
                            <strong style={{marginRight: '30px'}}>
                                {artist.name}
                            </strong>
                            <span style={{marginRight: '30px'}}>{artist.information}</span>
                            <NavLink to={'/artists/' + artist._id}>
                                <Button bsStyle="primary">Show album</Button>
                            </NavLink>
                        </Panel>
                    )
                })}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        artists: state.artists.artists
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchArtists: () => dispatch(fetchArtists())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);

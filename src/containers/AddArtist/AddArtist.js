import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addArtist, fetchArtists} from "../../store/actions/artistActions";
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class AddArtist extends Component {

    state = {
        name: '',
        image: null,
        information: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const artistData = new FormData();
        Object.keys(this.state).forEach(key => {
            artistData.append(key, this.state[key]);
        });
        this.props.addArtist(artistData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormGroup controlId="name">
                <Col componentClass={ControlLabel} sm={2}>
                    Name
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        required
                        placeholder="Enter name"
                        name="name"
                        value={this.state.name}
                        onChange={this.inputChangeHandler}
                    />
                </Col>
            </FormGroup>

                <FormGroup controlId="image">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            required
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="information">
                    <Col componentClass={ControlLabel} sm={2}>
                        Information
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="Enter information"
                            name="information"
                            value={this.state.information}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Add</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addArtist: (artist) => dispatch(addArtist(artist)),
        fetchArtists: () => dispatch(fetchArtists())
    }
};

export default connect(null, mapDispatchToProps)(AddArtist);
import React, {Fragment} from 'react';
import {Nav, Navbar, NavItem, NavDropdown, MenuItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Toolbar = ({user, logout}) => (
    <Navbar bsStyle="inverse">
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Music</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                <LinkContainer to="/" exact>
                    <NavItem>Artists</NavItem>
                </LinkContainer>

                <LinkContainer to="/history" exact>
                    <NavItem>Track history</NavItem>
                </LinkContainer>

                {user

                    ?

                    <Fragment>
                        <LinkContainer to="/register" exact>
                            <NavItem>Hello, {user.username}</NavItem>
                        </LinkContainer>


                            <NavItem onClick={logout}>Logout</NavItem>



                        <NavDropdown eventKey={3} title="Add new..." id="basic-nav-dropdown">
                            <MenuItem>
                                <LinkContainer to="/add-artist" exact>
                                    <NavItem>Add Artist</NavItem>
                                </LinkContainer>
                            </MenuItem>

                            <MenuItem>
                                <LinkContainer to="/create-album" exact>
                                    <NavItem>Add Album</NavItem>
                                </LinkContainer>
                            </MenuItem>

                            <MenuItem>
                                <LinkContainer to="/new-track">
                                    <NavItem>Add Track</NavItem>
                                </LinkContainer>
                            </MenuItem>
                        </NavDropdown>


                    </Fragment>

                    :

                    <Fragment>
                        <LinkContainer to="/register" exact>
                            <NavItem>Sign up</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/login" exact>
                            <NavItem>Login</NavItem>
                        </LinkContainer>
                    </Fragment>
                }

            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;
import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./containers/Layout/Layout";
import Register from './containers/Register/Register';
import Login from './containers/Login/Login';
import Artists from './containers/Artists/Artists';
import Album from './containers/Album/Album';
import Tracks from './containers/Tracks/Tracks';
import History from './containers/History/History';
import AddArtist from './containers/AddArtist/AddArtist';
import AddAlbum from './containers/AddAlbum/AddAlbum';
import AddTrack from './containers/AddTrack/AddTrack';

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Artists}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/history" component={History}/>
                    <Route path="/add-artist" component={AddArtist}/>
                    <Route path="/create-album" component={AddAlbum}/>
                    <Route path="/new-track" component={AddTrack}/>
                    <Route path="/artists/:id" component={Album}/>
                    <Route path="/albums/:id" component={Tracks}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
